const state = {
    User: []
};
const getters = {
    getUser(state) {
        return state.User
    },
};
const actions = {
    setUser({ commit }, res) {
        commit('User', res)
    }
};
const mutations = {
    User(state, res) {
        state.User = []
        return state.User = res
    }
};

export const User = {
    // namespaced: true,
    state,
    getters,
    actions,
    mutations
};
