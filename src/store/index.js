import Vue from "vue";
import Vuex from "vuex";
import {Building} from './Building.module'
import {Content} from './Content.module'
import {User} from './User.module'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {Building , Content, User},
});
