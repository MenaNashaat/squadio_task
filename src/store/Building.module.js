const state = {
    Building: null 
    
};
const getters = {
    getBuilding(state) {
        return state.Building
    },
};
const actions = {
    setBuilding({ commit }, res) {
        commit('Building', res)
    }
};
const mutations = {
    Building(state, res) {
        return state.Building = res
    }
};

export const Building = {
    // namespaced: true,
    state,
    getters,
    actions,
    mutations
};
