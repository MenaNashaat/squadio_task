const state = {
    Content: null
};
const getters = {
    getContent(state) {
        return state.Content
    },
};
const actions = {
    setContent({ commit }, res) {
        commit('Content', res)
    }
};
const mutations = {
    Content(state, res) {
        state.Content = []
        return state.Content = res
    }
};

export const Content = {
    // namespaced: true,
    state,
    getters,
    actions,
    mutations
};
