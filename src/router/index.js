import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/Building",
    name: "Building",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Building.vue"),
  },
  {
    path: "/EditBuilding",
    name: "EditBuilding",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/EditBuilding.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
